let chartOne = document.getElementById('chart1').getContext('2d');
let chartTwo = document.getElementById('chart2').getContext('2d');

// Chart.defaults.global.defaultFontFamily = 'monospace';
// Chart.defaults.global.defaultFontSize = 18;
// Chart.defaults.global.defaultFontColor = '#777';

let firstChart = new Chart(chartOne, {
  type: 'bar', // bar, pie, horizontalbar, doughnut, radar, line, polarArea
  data: {
    labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
    datasets: [{
      label: 'usage',
      data: [
        28,
        11,
        5,
        7,
        17,
        21,
        5,
        10,
        21,
        17,
        32,
        23
      ],
      backgroundColor: [
        'rgba(225, 99, 132, 0.6)',
        'rgba(54, 172, 235, 0.6)',
        'rgba(225, 206, 86, 0.6)',
        'rgba(75, 192, 192, 0.6)',
        'rgba(153, 106, 225, 0.6)',
        'rgba(225, 159, 64, 0.6)',
        'rgba(225, 99, 132, 0.6)',
        'rgba(54, 172, 235, 0.6)',
        'rgba(225, 206, 86, 0.6)',
        'rgba(75, 192, 192, 0.6)',
        'rgba(153, 106, 225, 0.6)',
        'rgba(225, 159, 64, 0.6)'
      ],
      borderWidth: 1,
      borderColor: '#777',
      hoverBorderWidth: 3,
      hoverBorderColor: '#000'
    }]
  },
  options: {
    title: {
      display: true,
      text: 'Largest cities in Massachusetts',
      fontSize: 25
    },
    legend: {
      display: false,
      position: 'right',
      labels: {
        fontColor: '#000'
      }
    },
    layout: {
      padding: {
        left: 40,
        right: 0,
        bottom: 0,
        top: 30
      },
      height: 200
    },
    tooltips: {
      enabled: true
    }
  }
}); 

console.log(chartOne);
let secondChart = new Chart(chartTwo, {
  type: 'doughnut', // bar, pie, horizontalbar, doughnut, radar, line, polarArea
  data: {
    labels: ['Mon', 'Tue', 'Wed', 'Thur', 'Fri', 'Sat', 'Sun'],
    datasets: [{
      label: 'usage',
      data: [
        5,
        9,
        13,
        4,
        16,
        10,
        8
      ],
      // backgroundColor: 'green'
      backgroundColor: [
        'rgba(225, 99, 132, 0.6)',
        'rgba(54, 172, 235, 0.6)',
        'rgba(225, 206, 86, 0.6)',
        'rgba(75, 192, 192, 0.6)',
        'rgba(153, 106, 225, 0.6)',
        'rgba(148, 56, 125, 0.6)',
        'rgba(90, 100, 155, 0.6)'
      ],
      borderWidth: 1,
      borderColor: '#777',
      hoverBorderWidth: 3,
      hoverBorderColor: '#000'
    }]
  },
  options: {
    title: {
      display: true,
      text: 'Largest cities in Massachusetts',
      fontSize: 25
    },
    legend: {
      display: false,
      position: 'right',
      labels: {
        fontColor: '#000'
      }
    },
    layout: {
      padding: {
        left: 35,
        right: 0,
        top: 40,
        bottom: 0
      }
    },
    tooltips: {
      enabled: true
    }
  }
}); 

